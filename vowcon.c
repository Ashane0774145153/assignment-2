#include <stdio.h>
int main() {
    char l;
    int simple, capital;
    printf("Enter the letter you want to check whether it's a vowel or consonant: ");
    scanf("%c", &l);

    simple = (l == 'a' || l == 'e' || l == 'i' || l == 'o' || l == 'u');

    capital = (l == 'A' || l == 'E' || l == 'I' || l == 'O' || l == 'U');

    if (simple || capital)
        printf("%c is a vowel \n", l);
    else
        printf("%c is a consonant \n", l);
    return 0;
}
